# Visual Studio Code CPP Template
I made this little thing for simplify development with CPP. 
This template have configured tasks for `cmake` and `make`. Also GDB working well.

Work only for Linux, 'cause on Windows better use Visual Studio.

## Requirements
1. Visual Studio Code;
2. VSCode CPP Extension;
3. CMake;
4. Make.

## Usage
1. Copy files from `vscode` to your `.vscode`.
2. Change in `.vscode/launch.json` value of key `program` to location of your executable in project folder. Example: `${workspaceRoot}/[your project name]`. Remember: if you set project name in CMakeLists.txt by `project(...)`, this name will be executable name.
3. Setup gcc. Add this to your CMakeLists.txt:

        # gcc flags (-g is for debug, -std=c++14 for C++ 2014 standart)
        if(CMAKE_COMPILER_IS_GNUCC)
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -g")
        endif(CMAKE_COMPILER_IS_GNUCC)

Also, you can add or remove some GCC/G++ arguments.

4. Now you ready for work with CPP code. Press `Ctrl+P`, enter `task` and select what you want to do. Hit `F5` to run your program.

Folder `vscode-cpp` is used for example. Just put `vscode` there and rename to `.vscode`. Open folder in Visual Studio Code and look how it works.